package site.availability.api.model;

public enum SourceType {
  TOPM, SEARCH, COMMON_CRAWL
}
