package site.availability.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.URL;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@JsonInclude(value = Include.NON_NULL)
public class Website {

  @URL
  @Indexed(unique = true)
  @NotNull
  private String url;
  private SourceType source = SourceType.TOPM;
  private String tld;
  private Boolean active;
  private String redirect;
}
