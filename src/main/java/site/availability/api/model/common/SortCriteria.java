package site.availability.api.model.common;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.util.StringUtils;

@Data
@Builder
public class SortCriteria {

  private String sortBy;
  private Direction order = Direction.DESC;

  public Sort sort() {
    if (StringUtils.isEmpty(sortBy)) {
      return Sort.unsorted();
    }
    return new Sort(order, sortBy);
  }
}
