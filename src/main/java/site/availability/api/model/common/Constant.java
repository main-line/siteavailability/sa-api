package site.availability.api.model.common;

import java.time.Instant;

public class Constant {

  public static final Instant EXPIRY_TIMESTAMP = Instant.parse("3000-01-01T01:01:01.01Z");

}

