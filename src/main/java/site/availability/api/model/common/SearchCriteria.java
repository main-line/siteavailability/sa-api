package site.availability.api.model.common;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class SearchCriteria {

  private String filter;
  private Integer pageSize = 100;
  private Integer page = 0;
  private List<String> fields = new ArrayList<>();

  public SearchCriteria(String filter) {
    this.filter = filter;
  }
}
