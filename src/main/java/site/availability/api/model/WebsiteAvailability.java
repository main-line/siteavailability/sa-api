package site.availability.api.model;

import static site.availability.api.model.common.Constant.EXPIRY_TIMESTAMP;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "availability")
@CompoundIndex(name = "url_start_end", def = "{'url' : 1, 'start' : 1, 'end' : 1}")
public class WebsiteAvailability {

  @JsonIgnore
  @ApiModelProperty(hidden = true)
  private String _id;
  @NotNull
  private String url;
  @NotNull
  private Instant start;
  @JsonProperty(access = Access.READ_ONLY)
  @NotNull
  private Instant end = EXPIRY_TIMESTAMP;
  private List<Instant> timePoints;
  private Integer code;
  private String redirect;
}
