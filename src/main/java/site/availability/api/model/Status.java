package site.availability.api.model;

import java.time.Instant;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class Status {

  private Instant time;
  private HttpStatus code;
}
