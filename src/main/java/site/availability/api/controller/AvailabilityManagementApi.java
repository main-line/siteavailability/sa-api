package site.availability.api.controller;

import java.util.List;
import javax.annotation.Resource;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import site.availability.api.dao.AvailabilityDao;
import site.availability.api.model.WebsiteAvailability;
import site.availability.api.model.common.PageResult;
import site.availability.api.model.common.SearchCriteria;

@RestController
public class AvailabilityManagementApi {

  @Resource
  private AvailabilityDao availabilityDao;

  @GetMapping("/availability")
  public PageResult<WebsiteAvailability> search(SearchCriteria searchCriteria) {
    return availabilityDao.search(searchCriteria);
  }


  @PutMapping("/timeseries/availability")
  public void put(@RequestBody @Valid WebsiteAvailability websiteAvailability) {
    availabilityDao.put(websiteAvailability);
  }


  @PutMapping("/timeseries/bulk/availability")
  public void put(@RequestBody List<WebsiteAvailability> websiteAvailabilityList) {
    websiteAvailabilityList
        .forEach(websiteAvailability -> availabilityDao.put(websiteAvailability));
  }

}
