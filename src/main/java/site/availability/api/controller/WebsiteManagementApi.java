package site.availability.api.controller;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import site.availability.api.dao.WebsiteDao;
import site.availability.api.model.Website;
import site.availability.api.model.common.PageResult;
import site.availability.api.model.common.SearchCriteria;

@RestController
public class WebsiteManagementApi {

  @Resource
  private WebsiteDao websiteDao;

  @PostMapping("/website")
  public Website post(@RequestBody Website website) {
    return websiteDao.insert(website);
  }

  @PutMapping("/website/bulk")
  public void upsert(@RequestBody List<Website> website) {
    websiteDao.update(website);
  }

  @GetMapping("/website")
  public PageResult<Website> search(SearchCriteria searchCriteria) {
    return websiteDao.search(searchCriteria);
  }
}
