package site.availability.api.query;

import com.github.rutledgepaulv.qbuilders.builders.GeneralQueryBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.visitors.MongoVisitor;
import com.github.rutledgepaulv.rqe.pipes.QueryConversionPipeline;
import java.time.Instant;
import java.util.Objects;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import site.availability.api.model.common.SearchCriteria;
import site.availability.api.model.common.SortCriteria;

public class MongoQueryCreator {

  private static QueryConversionPipeline pipeline = QueryConversionPipeline.defaultPipeline();
  private static MongoVisitor mongoVisitor = new MongoVisitor();

  public static Query create(SearchCriteria searchCriteria,
      SortCriteria sortCriteria, Class<?> targetEntity) {
    Criteria criteria = new Criteria();

    if (Objects.nonNull(searchCriteria.getFilter())) {
      Condition<GeneralQueryBuilder> condition = pipeline
          .apply(searchCriteria.getFilter(), targetEntity);
      criteria = condition.query(mongoVisitor);
    }

    Query query = Query.query(criteria)
        .with(sortCriteria.sort())
        .limit(searchCriteria.getPageSize())
        .skip(searchCriteria.getPage() * searchCriteria.getPageSize());

    addFields(query, searchCriteria);
    return query;
  }

  public static Query buildTimeSeriesQuery(Instant start, Instant end, String key, String value) {
    return Query.query(Criteria.where("start").lte(start).and("end").gt(end).and(key).is(value));
  }

  public static Query buildSimpleKeyValueQuery(String key, Object value) {
    return Query.query(Criteria.where(key).is(value));
  }

  public static Update buildSimpleKeyValueUpdate(String key, Object value) {
    return new Update().set(key, value);
  }

  private static void addFields(Query query, SearchCriteria searchCriteria) {
    searchCriteria.getFields().forEach(field -> query.fields().include(field));
  }
}
