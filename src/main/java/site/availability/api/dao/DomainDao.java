package site.availability.api.dao;

import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import site.availability.api.model.Collection;
import site.availability.api.model.common.PageResult;
import site.availability.api.model.common.SearchCriteria;
import site.availability.api.model.common.SortCriteria;
import site.availability.api.query.MongoQueryCreator;

@Component
@Slf4j
public class DomainDao<T> {

  @Resource
  private MongoTemplate mongoTemplate;

  public T insert(T object, Collection collection) {
    return mongoTemplate.insert(object, collection.name());
  }

  public <T> java.util.Collection<T> insertMany(List<T> objects) {
    return mongoTemplate.insertAll(objects);
  }

  PageResult<T> search(SearchCriteria searchCriteria, SortCriteria sortCriteria,
      Class<T> targetEntity, Collection collection) {
    Query query = MongoQueryCreator.create(searchCriteria, sortCriteria, targetEntity);
    List<T> returnObjects = get(query, targetEntity, collection);
    long count = mongoTemplate.count(query, targetEntity, collection.name());
    return new PageResult<T>().of(returnObjects)
        .page(searchCriteria.getPage())
        .pageSize(searchCriteria.getPageSize())
        .totalCount(count);
  }

  List<T> get(Query query, Class<T> targetEntity, Collection collection) {
    log.info("Search Query : {}, collection: {}", query, collection.name());
    return mongoTemplate.find(query, targetEntity, collection.name());
  }

  T patch(Query query, Update update, Class<T> targetEntity, Collection collection) {
    log.info("Patch Query : {}, collection: {}", query, collection.name());
    return mongoTemplate.findAndModify(query, update, targetEntity, collection.name());
  }


}
