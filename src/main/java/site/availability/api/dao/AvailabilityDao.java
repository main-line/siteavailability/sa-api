package site.availability.api.dao;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import site.availability.api.model.Collection;
import site.availability.api.model.WebsiteAvailability;
import site.availability.api.model.common.PageResult;
import site.availability.api.model.common.SearchCriteria;
import site.availability.api.model.common.SortCriteria;
import site.availability.api.query.MongoQueryCreator;

@Component
public class AvailabilityDao {

  @Resource
  private DomainDao<WebsiteAvailability> domainDao;
  @Resource
  private WebsiteDao websiteDao;

  public PageResult<WebsiteAvailability> search(SearchCriteria searchCriteria) {
    return domainDao.search(searchCriteria, SortCriteria.builder().build()
        , WebsiteAvailability.class, Collection.availability);
  }


  public void put(WebsiteAvailability websiteAvailability) {
    Query query = MongoQueryCreator.buildTimeSeriesQuery(websiteAvailability.getStart(),
        websiteAvailability.getStart(), "url", websiteAvailability.getUrl());
    List<WebsiteAvailability> previousVersion = domainDao
        .get(query, WebsiteAvailability.class, Collection.availability);
    if (previousVersion.isEmpty()) {
      domainDao.insert(websiteAvailability, Collection.availability);
    } else {
      if (!previousVersion.get(0).getCode().equals(websiteAvailability.getCode())) {

        Update update = Update.update("end", websiteAvailability.getStart());
        Query updateQuery = new Query()
            .addCriteria(Criteria.where("_id").is(previousVersion.get(0).get_id()));
        domainDao.patch(updateQuery, update, WebsiteAvailability.class, Collection.availability);
        domainDao.insert(websiteAvailability, Collection.availability);
      } else {
        Update update = new Update().addToSet("timePoints", websiteAvailability.getStart());
        Query updateQuery = new Query()
            .addCriteria(Criteria.where("_id").is(previousVersion.get(0).get_id()));
        domainDao.patch(updateQuery, update, WebsiteAvailability.class, Collection.availability);
      }
    }
    websiteDao.patch(websiteAvailability);
  }
}
