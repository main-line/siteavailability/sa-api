package site.availability.api.dao;

import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import site.availability.api.model.Collection;
import site.availability.api.model.Website;
import site.availability.api.model.WebsiteAvailability;
import site.availability.api.model.common.PageResult;
import site.availability.api.model.common.SearchCriteria;
import site.availability.api.model.common.SortCriteria;
import site.availability.api.query.MongoQueryCreator;

@Component
@Slf4j
public class WebsiteDao {

  @Resource
  private DomainDao<Website> domainDao;

  public Website insert(Website website) {
    return domainDao.insert(website, Collection.website);
  }

  public void update(List<Website> websites) {
    domainDao.insertMany(websites);
  }

  public PageResult<Website> search(SearchCriteria searchCriteria) {
    return domainDao.search(searchCriteria, SortCriteria.builder().build()
        , Website.class, Collection.website);
  }

  public void patch(WebsiteAvailability websiteAvailability) {
    Update update = MongoQueryCreator
        .buildSimpleKeyValueUpdate("redirect", websiteAvailability.getRedirect());
    Query query = MongoQueryCreator.buildSimpleKeyValueQuery("url", websiteAvailability.getUrl());
    domainDao.patch(query, update, Website.class, Collection.website);
  }

}
