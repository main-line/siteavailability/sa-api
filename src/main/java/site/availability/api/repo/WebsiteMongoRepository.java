package site.availability.api.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import site.availability.api.model.Website;

@Component
public interface WebsiteMongoRepository extends MongoRepository<Website, String> {

}
