package site.availability.api.query;


import static org.mockito.Mockito.when;
import static org.springframework.data.domain.Sort.Direction.ASC;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Query;
import site.availability.api.model.WebsiteAvailability;
import site.availability.api.model.common.SearchCriteria;
import site.availability.api.model.common.SortCriteria;

@RunWith(MockitoJUnitRunner.class)
public class MongoQueryCreatorTest {

  @Mock
  private SearchCriteria mockSearchCriteria;
  @Mock
  private SortCriteria mockSortCriteria;

  @Test
  public void test_query_creation_correctly() {
    //given
    when(mockSearchCriteria.getFilter()).thenReturn("url==google.com");
    when(mockSearchCriteria.getPageSize()).thenReturn(100);
    when(mockSearchCriteria.getPage()).thenReturn(0);
    when(mockSortCriteria.sort()).thenReturn(new Sort(ASC, "dateTime"));

    //when
    Query query = MongoQueryCreator
        .create(mockSearchCriteria, mockSortCriteria, WebsiteAvailability.class);

    //then
    Assert.assertEquals(query.toString(),
        "Query: { \"url\" : \"google.com\" }, Fields: { }, Sort: { \"dateTime\" : 1 }");

  }

}