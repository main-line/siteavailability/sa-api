package site.availability.api.dao;


import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import site.availability.api.model.Collection;
import site.availability.api.model.Website;

@RunWith(MockitoJUnitRunner.class)
public class WebsiteDaoTest {

  @Mock
  private DomainDao<Website> mockDomainDao;
  @Mock
  private Website mockWebsite;
  @InjectMocks
  private WebsiteDao websiteDao;


  @Test
  public void insert_object() {
    //given
    when(mockDomainDao.insert(mockWebsite, Collection.website)).thenReturn(mockWebsite);

    //when
    Website result = websiteDao.insert(mockWebsite);

    //then
    verify(mockDomainDao).insert(mockWebsite, Collection.website);
  }
}