package site.availability.api.controller;


import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import site.availability.api.dao.WebsiteDao;
import site.availability.api.model.Website;

@RunWith(MockitoJUnitRunner.class)
public class WebsiteManagementApiTest {

  @InjectMocks
  private WebsiteManagementApi websiteManagementApi;
  @Mock
  private WebsiteDao mockWebsiteDao;
  @Mock
  private Website mockWebsite;

  @Test
  public void check_interactions_happening_correctly() {
    //given
    when(mockWebsiteDao.insert(mockWebsite)).thenReturn(mockWebsite);

    //when
    websiteManagementApi.post(mockWebsite);

    //then
    verify(mockWebsiteDao).insert(mockWebsite);
  }

}