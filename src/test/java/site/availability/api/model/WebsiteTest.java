package site.availability.api.model;

import static org.junit.Assert.assertEquals;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class WebsiteTest {

  private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();


  @Test
  public void validate_website_domain_object() {
    //given
    Website website = new Website();
    website.setUrl("google.cok");

    //when
    Set<ConstraintViolation<Website>> errors = validator.validate(website);

    //then
    assertEquals(1, errors.size());

  }

}
