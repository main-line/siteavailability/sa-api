package site.availability.api.model;

import org.junit.Assert;
import org.junit.Test;
import site.availability.api.model.common.Constant;

public class WebsiteAvailabilityTest {

  @Test
  public void test_website_availability_domain() {

    WebsiteAvailability websiteAvailability = new WebsiteAvailability();

    Assert.assertEquals(websiteAvailability.getEnd(), Constant.EXPIRY_TIMESTAMP);

  }

}